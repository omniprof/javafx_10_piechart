package com.kenfogel.javafx_10_piechart;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.chart.*;
import javafx.scene.layout.Pane;

/**
 * Adapted from
 * https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/charts.htm
 *
 * @author Ken
 */
public class MainApp extends Application {

    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Imported Fruits");

        int width = 500;
        int height = 500;

        Pane root = new Pane();

        root.getChildren().add(createPieChart());
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    /**
     * Create a Pie Chart
     *
     * @return
     */
    private PieChart createPieChart() {
        ObservableList<PieChart.Data> pieChartData
                = FXCollections.observableArrayList(
                        new PieChart.Data("Grapefruit", 13),
                        new PieChart.Data("Oranges", 25),
                        new PieChart.Data("Plums", 10),
                        new PieChart.Data("Pears", 22),
                        new PieChart.Data("Apples", 30));
        final PieChart chart = new PieChart(pieChartData);
        chart.setTitle("Imported Fruits");
        return chart;
    }

    /**
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
